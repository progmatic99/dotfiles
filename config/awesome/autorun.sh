#!/usr/bin/env bash

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

run thunar --daemon
feh --no-fehbg --bg-fill ~/wallpapers/anime/sword-red.png &
run picom --experimental-backends -b
