-- Standard awesome library
local gears = require("gears")
local awful = require("awful")

local assault = require("assault")

-- Wibox handling library
local wibox = require("wibox")

-- Custom Local Library: Common Functional Decoration
local deco = {
  wallpaper = require("deco.wallpaper"),
  taglist   = require("deco.taglist"),
  tasklist  = require("deco.tasklist")
}

local taglist_buttons  = deco.taglist()
local tasklist_buttons = deco.tasklist()

local _M = {}

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()

-- Create the battery widget
myassault = assault({
  battery = "BAT0", -- battery ID to get data from
  adapter = "AC", -- ID of the AC adapter to get data from
  width = 32, -- width of battery
  height = 13, -- height of battery
  bolt_width = 19, -- width of charging bolt
  bolt_height = 11, -- height of charging bolt
  stroke_width = 2, -- width of battery border
  peg_top = (calculated), -- distance from the top of the battery to the start of the peg
  peg_height = 5, -- height of the peg
  peg_width = 2, -- width of the peg
  critical_level = 0.15, -- battery percentage to mark as critical (between 0 and 1, default is 10%)
  normal_color = "#90ee90", -- color to draw the battery when it's discharging
  critical_color = "#DC143C", -- color to draw the battery when it's at critical level
  charging_color = "#00ff00" -- color to draw the battery when it's charging
 })


awful.screen.connect_for_each_screen(function(s)
  -- Wallpaper
  set_wallpaper(s)

  -- Create a promptbox for each screen
  s.mypromptbox = awful.widget.prompt()

  -- Create an imagebox widget which will contain an icon indicating which layout we're using.
  -- We need one layoutbox per screen.
  s.mylayoutbox = awful.widget.layoutbox(s)
  s.mylayoutbox:buttons(gears.table.join(
    awful.button({ }, 1, function () awful.layout.inc( 1) end),
    awful.button({ }, 3, function () awful.layout.inc(-1) end),
    awful.button({ }, 4, function () awful.layout.inc( 1) end),
    awful.button({ }, 5, function () awful.layout.inc(-1) end)
  ))

  -- Create a taglist widget
  s.mytaglist = awful.widget.taglist {
    screen  = s,
    filter  = awful.widget.taglist.filter.all,
    buttons = taglist_buttons
  }

  -- Create a tasklist widget
  s.mytasklist = awful.widget.tasklist {
    screen  = s,
    filter  = awful.widget.tasklist.filter.currenttags,
    buttons = tasklist_buttons
  }

  -- Create the wibox
  s.mywibox = awful.wibar({ position = "top", screen = s })

  -- Add widgets to the wibox
  s.mywibox:setup {
    layout = wibox.layout.align.horizontal,
    { -- Left widgets
      layout = wibox.layout.fixed.horizontal,
      RC.launcher,
      s.mytaglist,
      s.mypromptbox,
    },
    s.mytasklist, -- Middle widget
    { -- Right widgets
      layout = wibox.layout.fixed.horizontal,
      mykeyboardlayout,
      wibox.widget.systray(),
      mytextclock,
      myassault,
      redshift,
      s.mylayoutbox,
    },
  }
end)
-- }}}
