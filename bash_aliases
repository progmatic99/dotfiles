# enable color support of ls and also add handy aliases
# if [ -x /usr/bin/dircolors ]; then
#     test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
#     alias ls='ls --color=auto'
#     #alias dir='dir --color=auto'
#     #alias vdir='vdir --color=auto'
#
#     alias grep='grep --color=auto'
#     alias fgrep='fgrep --color=auto'
#     alias egrep='egrep --color=auto'
# fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -a'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# User specific aliases and functions
alias e='exit'
alias c='clear'
alias tm='tmux'
alias py='python3'
alias ..='cd ..'
alias ...='cd ../../'

### Web Dev aliases ###
alias cra='create-react-app'
alias yr='yarn'
alias yd='yarn dev'

#######################

alias dcubd='docker-compose up --build -d'
alias dcud='docker-compose up -d'
alias dci='docker images'
alias dcps='docker ps'
alias dcs='docker-compose stop'
alias bejs='bundle exec jekyll serve'
alias jes='jekyll serve'
alias ghs='ghostwriter'

# Git command alias
alias gcl='git clone'
alias ga='git add'
alias gcm='git commit'
alias gbr='git branch'
alias gch='git checkout'
alias gs='git status'
alias glp="git log --topo-order --all --graph --date=local --pretty=format:'%C(green)%h%C(reset) %><(55,trunc)%s%C(red)%d%C(reset) %C(blue)[%an]%C(reset) %C(yellow)%ad%C(reset)%n'"
alias t='tree'
alias gpom='git push origin master'
alias gpl='git pull'
alias grv='git remote -v'
alias gl='git log'
alias gd='git diff'
alias glol='git log --oneline'
alias v='vim'
alias n='nvim'
alias m='make'

# Ember commands
alias em='ember'

# Django aliases
alias prs='py manage.py runserver'
alias pmm='py manage.py migrate'

# npm
alias nr='npm run'
alias nrb='nr build'

# to launch ssh-agent using X
alias startx='ssh-agent startx'
