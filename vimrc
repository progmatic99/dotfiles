" To check if my vimrc is loaded

set nocompatible
filetype off

" Fuzzy finder for files in sub-dirs
set path+=**

" Always show tabline
set showtabline=2

" Vundle config
set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin('~/.vim/Plugins')

" Plugins here
Plugin 'itchyny/lightline.vim'
Plugin 'mengelbrecht/lightline-bufferline'
Plugin 'w0rp/ale'
Plugin 'EdenEast/nightfox.nvim'
Plugin 'sheerun/vim-polyglot'
Plugin 'maximbaz/lightline-ale'
Plugin 'itchyny/vim-gitbranch'

call vundle#end()

" Mode doesn't appear at the bottom
set noshowmode

" splitline customization
" set fillchars+=vert:\|

" No swap file is created
set noswapfile

set termguicolors
"let ayucolor="dark"
"set background=dark
"let g:gruvbox_contrast_dark='hard'

"let g:vim_monokai_tasty_italic = 1
"let g:material_style = "darker"
colorscheme duskfox

" Checks filetype for proper indentation
filetype plugin indent on

" Show relative line number
set number
set rnu

" Syntax Highlighting ON
syntax on

" Display Line Numbers
set ruler

" Set fold lines using F9
set foldmethod=manual

" Template Files for different extensions
autocmd BufNewFile *.c 0r ~/.vim/templates/template.c
    au BufNewFile *.cpp 0r ~/.vim/templates/template.cpp
    au BufNewFile *.cpp normal 17j
augroup END
autocmd BufNewFile *.html 0r ~/.vim/templates/template.html
autocmd BufNewFile *.go 0r ~/.vim/templates/template.go
au FileType cpp
	\ nnoremap <C-p> :bo 32vs ./output.txt <CR>:split ./input.txt <CR> :w<CR> <C-w>h |
"	\ nnoremap <C-o> :%bd\|e#<CR>

set expandtab

" set tabs to spaces
set tabstop=2
set softtabstop=2

set shiftwidth=2

" For 80 characters per line
" highlight ColorColumn ctermbg=red
" call matchadd('ColorColumn', '\%81v', 100)

" Status bar at bottom
set laststatus=2
set statusline=
set statusline+=\ %f

" Show result of tab
set wildmenu

" Tweaks for browsing
let g:netrw_banner=0     " disable annoying banner

if !has('gui_running')
    set t_Co=256
endif

""""""""""""""""""""""""""
" Mappings
""""""""""""""""""""""""""

let mapleader = ";"

" Changing material theme variants
nnoremap <leader>mm :lua require('material.functions').toggle_style()<CR>
" Saving only
nnoremap <leader>s :w<Enter>
" Quitting vim
nnoremap <leader>q :q<Enter>
" Vex
nnoremap <leader>e :Vex!<Enter>
" Quit all
nnoremap <leader>x :qa<Enter>
" Copy to system clipboard
nnoremap <leader>c :w !xclip -sel clip<Enter>
" Move right/left/down/up in panes
nnoremap <leader>l <C-W>l
nnoremap <leader>h <C-W>h
nnoremap <leader>j <C-W>j
nnoremap <leader>k <C-W>k
" Adding a line above/below the current line
noremap <leader><CR> O<Esc>j
noremap <leader>o o<Esc>k
" Maximizing
nnoremap <leader>+ :exe "resize " . (winheight(0) * 3/2)<CR>

nnoremap <leader>= <C-W>=

" Buffer movements
nnoremap <leader>d :bn<CR>
nnoremap <leader>a :bp<CR>

" Minimizing
nnoremap <leader>- :exe "resize " . (winheight(0) * 2/3)<CR>

"Run & Compile C/C++
nnoremap <leader>r :make %<<CR> :!./%<<CR>

" Telescope nvim
" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

let g:ale_completion_enabled = 1
let g:mustache_abbreviations = 1

let g:lightline = {}
let g:lightline = {
    \ 'colorscheme': 'duskfox',
    \ 'active': {
    \ 'left': [ [ 'mode', 'paste' ],
    \           [ 'gitbranch', 'readonly', 'filename', 'modified' ] ],
    \ 'right': [ [ 'lineinfo' ],
    \            [ 'percent' ],
    \            [ 'fileformat', 'fileencoding', 'filetype' ],
    \            [ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_infos', 'linter_ok' ], ] },
    \ 'component_function': {
    \   'gitbranch': 'gitbranch#name'
    \ }, }
let g:lightline.component_expand = {
      \  'linter_checking': 'lightline#ale#checking',
      \  'linter_infos': 'lightline#ale#infos',
      \  'linter_warnings': 'lightline#ale#warnings',
      \  'linter_errors': 'lightline#ale#errors',
      \  'linter_ok': 'lightline#ale#ok',
      \ }
let g:lightline.component_type = {
      \     'linter_checking': 'right',
      \     'linter_infos': 'right',
      \     'linter_warnings': 'warning',
      \     'linter_errors': 'error',
      \     'linter_ok': 'right',
      \ }
"let g:lightline.active = {
"    \ 'left': [ [ 'mode', 'paste' ],
"    \           [ 'gitbranch', 'readonly', 'filename', 'modified' ] ],
"    \ 'right': [ [ 'lineinfo' ],
"    \            [ 'percent' ],
"    \            [ 'fileformat', 'fileencoding', 'filetype' ],
"    \            [ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_infos', 'linter_ok' ], ] }
let g:lightline.inactive = {
    \ 'left': [ [ 'filename' ] ],
    \ 'right': [ [ 'lineinfo' ],
    \            [ 'percent' ] ] }
let g:lightline.tabline = {
    \ 'left': [ [ 'tabs' ] ],
    \ 'right': [  ] }
let g:lightline.tabs = {
    \ 'active': [ 'tabnum', 'filename', 'modified'],
    \ 'inactive': [ 'tabnum', 'filename', 'modified'] }


""""""""""""""""""""""""""""
" Ale settings
""""""""""""""""""""""""""""
let g:ale_linters = {
    \ 'python': ['flake8'],
    \ 'javascript': ['eslint'],
    \ 'go': ['gofmt'] }
let g:ale_fixers = {
    \ '*': ['remove_trailing_lines', 'trim_whitespace'],
    \ 'python': ['black'] }
let g:ale_fix_on_save = 1

"""""""""""""""""""""""""
" Netrw settings
"""""""""""""""""""""""""

" Tree list style
let g:netrw_liststyle=3

" Opens file in vertical split
let g:netrw_browse_split=2

" Width of directory explorer
let g:netrw_winsize=20

" Disable mouse in console based vim
if has('nvim')
    " Neovim specific commands
else
    " Standard vim specific commands
    set mouse=
    set ttymouse=
endif

" Look for tags in cwd, then work upto root
" until found
set tags=./tags;/
