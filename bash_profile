#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

# GO-Path
# export PATH=$PATH:/usr/local/go/bin
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"
# For starting startx
[[ $(fgconsole 2>/dev/null) == 1 ]] && exec startx -- vt1
. "$HOME/.cargo/env"
export VOLTA_HOME="$HOME/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"
